/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.Timer;
import modelo.Nodo;
/**
 *
 * @author USAF
 */
public class Graphic extends JPanel implements ActionListener{
    private Nodo[] banderas_ejecucion;
    private Timer temporizador;
    private int tiempo;
    
    @Override
    public void paintComponent(Graphics g){
        if(tiempo==-1){
            super.paintComponent(g);
        }
        if (banderas_ejecucion==null){
            g.setColor(Color.blue);
            g.drawOval(tiempo, tiempo, 40, 40);
        }
        else{
            tiempo++;
            //pintar divisiones
            g.drawString(""+tiempo, 100+tiempo*25, 100);
            g.drawLine(100+tiempo*25, 120,100+tiempo*25, this.getHeight()-10);

            if(tiempo+1 == banderas_ejecucion[banderas_ejecucion.length-1].tiempo_final.get(banderas_ejecucion[banderas_ejecucion.length-1].tiempo_final.size()-1)){
                g.drawString(""+(tiempo+1), 100+(tiempo+1)*25, 100);
                g.drawLine(100+(tiempo+1)*25, 120,100+(tiempo+1)*25, this.getHeight()-10);
            }
            for(int i = 0; i < banderas_ejecucion.length;i++){
                if(tiempo == 0){
                    g.setColor(Color.blue);
                    g.drawString("P-"+banderas_ejecucion[i].id, 50, 150+10*i);
                }
                if(banderas_ejecucion[i].ejecución.get(tiempo)==null){
                        
                }
                else if(banderas_ejecucion[i].ejecución.get(tiempo)==true){
                    g.setColor(Color.GREEN);
                    g.fillRect(100+tiempo*25, 150+10*i, 25, 10);
                    //System.out.println(banderas_ejecucion[i].ejecución.toString());
                }
                else if(banderas_ejecucion[i].ejecución.get(tiempo)==false){                      
                    g.setColor(Color.BLUE);
                    g.fillRect(100+tiempo*25, 150+10*i, 25, 10);
                }
            }
            if(!temporizador.isRunning()){
                temporizador.start();
            }
            if(tiempo+1 == banderas_ejecucion[banderas_ejecucion.length-1].tiempo_final.get(banderas_ejecucion[banderas_ejecucion.length-1].tiempo_final.size()-1)){
                temporizador.stop();
                tiempo=-1;
            }
            
        }

    }
    
    public void setBaderas_ejecucion(Nodo[] baderas_ejecucion) {
        this.banderas_ejecucion = baderas_ejecucion;
    }

    public Graphic(){
        banderas_ejecucion = null;
        tiempo = -1;
        temporizador = new Timer(500,this);
        temporizador.setActionCommand("tiempo");
        //
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("tiempo")){
            this.repaint();
        }
    }
}
