/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;
import Controlador.Controlador;
import java.awt.HeadlessException;
import java.util.HashSet;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author USAF
 */
public class Vista extends JFrame{
    private JTable tabla;
    private Graphic grafico;
    private JButton boton_numprocesos;
    private JTextField campo_numprocesos;
    private Controlador controlador;
    private JScrollPane scrollPane;
    private DefaultTableModel model;

    
    public Vista(Controlador controlador) throws HeadlessException {
        this.controlador = controlador;
        this.setSize(1200, 700);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Procesamiento Expulsivo");
        this.setResizable(false);
        
        //MODELO DE LA TABLA
        this.model = new DefaultTableModel();
        this.model.addColumn("Identificador");
        this.model.addColumn("Tiempo Llegada");
        this.model.addColumn("Tiempo Rafaga");
        this.model.addColumn("Tiempo Inicio");
        this.model.addColumn("Tiempo Final");
        this.model.addColumn("Tiempo Espera");
        this.model.addColumn("Tiempo Retorno");
        this.tabla = new JTable(model);

        
        //PANEL, SCROLL PANE, JTEXTAREA Y BOTON INICIAR
        this.grafico = new Graphic();
        this.grafico.setBounds(15, 15, 770, 300);
        this.scrollPane = new JScrollPane(tabla);
        this.scrollPane.setBounds(20, 400, 1000, 400);
        this.boton_numprocesos = new JButton ("Iniciar");
        this.boton_numprocesos.setBounds(1000, 25, 150, 25);
        this.boton_numprocesos.addActionListener(controlador);
        this.boton_numprocesos.setActionCommand("iniciar");
        this.campo_numprocesos = new JTextField("");
        this.campo_numprocesos.setBounds(800,25,150,25);
        
        this.add(this.scrollPane);
        this.add(this.grafico);
        this.add(this.boton_numprocesos);
        this.add(this.campo_numprocesos);
        this.setLayout(null);
    }


    public Graphic getGrafico() {
        return grafico;
    }

    public void setGrafico(Graphic grafico) {
        this.grafico = grafico;
    }
    
    public void generarGraficosGantt(){
        grafico.repaint();
    }
    
    public DefaultTableModel getModel() {
        return model;
    }

    public JTextField getCampo_numprocesos() {
        return campo_numprocesos;
    }
    
    public void actualizar_tabla(Object[][] datos){
        this.model.addRow(datos);
    }
    
}
