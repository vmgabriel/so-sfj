/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.Timer;
import modelo.Nodo;
import modelo.Simulador;
import vista.Graphic;
import vista.Vista;

/**
 *
 * @author USAF
 */
public class Controlador implements ActionListener{
    private Vista vista_principal;
    private Simulador simulador;
    
    public Controlador(){
        vista_principal = new Vista(this);
    }
    
    public void iniciarVista(){
        vista_principal.setVisible(true);     
    }
    
    
    public void actualizar_tabla(Object [] datos){
        vista_principal.getModel().addRow(datos);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getActionCommand().equals("iniciar")){
            try{
                simulador = new Simulador();
                vista_principal.getModel().setNumRows(0);

                int num_procesos = Integer.parseInt(vista_principal.getCampo_numprocesos().getText());
                simulador.simular_procesos_expulsivos(num_procesos);
                ArrayList<Nodo> lista_finalizados = new ArrayList();
                lista_finalizados = simulador.obtener_finalizados();
                for (int i = 0; i < lista_finalizados.size();i++){
                    actualizar_tabla(new Object[]{lista_finalizados.get(i).id,lista_finalizados.get(i).tiempo_llegada,lista_finalizados.get(i).rafaga
                    ,lista_finalizados.get(i).tiempo_de_comienzo,lista_finalizados.get(i).tiempo_final,lista_finalizados.get(i).tiempo_de_espera
                    ,lista_finalizados.get(i).tiempo_de_retorno});
                    
                }
                vista_principal.getGrafico().setBaderas_ejecucion(lista_finalizados.toArray(new Nodo[lista_finalizados.size()]));
                vista_principal.generarGraficosGantt();
            }catch(Exception n){
                System.err.println(n);
            }
        }
    }
    
}
