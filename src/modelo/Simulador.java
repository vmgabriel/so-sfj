/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;

/**
 *
 * @author USAF
 */
public class Simulador {
    private Cola cola_de_procesos;
    private int tiempo;
    public Simulador() {
        cola_de_procesos = new Cola();
    }
    
    public void generar_cola_procesos(int num_procesos){
        for (int i = 0;i < num_procesos; i++){
            cola_de_procesos.insertar_proceso(); //generación de procesos
        }
    }
    
    public void simular_procesos_expulsivos(int num_procesos){
        generar_cola_procesos(num_procesos);
        cola_de_procesos.imprimir_procesos_test();
        //System.out.println("Iniciando Simulación...");
        
        int tiempo_final = cola_de_procesos.getNodo().get(cola_de_procesos.getNodo().size()-1).tiempo_llegada + cola_de_procesos.getNodo().get(cola_de_procesos.getNodo().size()-1).rafaga.get(0);
        //System.out.println("Tiempo final : "+tiempo_final);
        Nodo aux = cola_de_procesos.getNodo().get(0);
        aux.tiempo_de_comienzo.add(0);
        Nodo aux_2 = null;
        tiempo = 0;
        while (cola_de_procesos.getNodo().size()>0){
            cola_de_procesos.reordenarColaNodo(tiempo);
            
            
            

            //CONTROL DE TIEMPO DE COMIENZO Y FINAL
            if (aux != cola_de_procesos.getNodo().get(0)){
                aux.tiempo_final.add(tiempo);
                aux = cola_de_procesos.getNodo().get(0);
                if (aux.tiempo_llegada > tiempo){
                    aux.tiempo_de_comienzo.add(aux.tiempo_llegada);
                }
                else{
                    aux.tiempo_de_comienzo.add(tiempo);
                }
                if(cola_de_procesos.getNodo().size()==1){
                    aux.tiempo_final.add(tiempo+aux.rafaga.get(aux.rafaga.size()-1));
                }
            }
            /////////////////////////////
            //CONTROL DE RAFAGA
        if (cola_de_procesos.getNodo().get(0).tiempo_llegada <= tiempo){
                try{
                   aux_2 = cola_de_procesos.getNodo().get(0);
                   if (aux_2.rafaga.get(aux_2.rafaga.size()-1)>0){
                        aux_2.rafaga.add(aux_2.rafaga.get(aux_2.rafaga.size()-1)-1);
                    }
                }catch(Exception e){
                    System.err.println("Error en control rafaga "+e);
                }
            }
            cola_de_procesos.añadir_finalizados();
            tiempo++;
        }
       // System.out.println("Finalizado....");
        cola_de_procesos.definir_variables_restantes();
        cola_de_procesos.generar_banderas_ejecucion();
        //System.out.println("Definidas....");
       //cola_de_procesos.imprimir_procesos_finalizados_test();
    }
    
    
    public ArrayList<Nodo> obtener_finalizados(){
        return cola_de_procesos.getFinalizado();
    } 
}
