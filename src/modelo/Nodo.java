/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
import java.util.ArrayList;
/**
 *
 * @author USAF
 */
public class Nodo {
    public int id;
    public int tiempo_llegada;
    public ArrayList<Integer> rafaga;
    public ArrayList<Integer> tiempo_de_comienzo;
    public ArrayList<Integer> tiempo_final;
    public ArrayList<Boolean> ejecución;
    public int tiempo_de_retorno;
    public int tiempo_de_espera;

    public Nodo(int id,int rafaga, int tiempo_llegada) {
        this.id = id;
        this.rafaga = new ArrayList();
        this.rafaga.add(rafaga);
        this.tiempo_llegada = tiempo_llegada;
        this.tiempo_de_comienzo = new ArrayList();
        this.tiempo_final = new ArrayList();
        this.ejecución = new ArrayList();
    }
    

}
