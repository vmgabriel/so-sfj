/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author USAF
 */
public class Cola {
    private ArrayList<Nodo> ejecucion;
    private ArrayList<Nodo> finalizado;
    private int tiempo_llegada;
    
    Cola(){
        tiempo_llegada = 0;
        ejecucion = new ArrayList();
        finalizado = new ArrayList();
    }
    public ArrayList<Nodo> getNodo() {
        return ejecucion;
    }
    
    public ArrayList<Nodo> getFinalizado() {
        return finalizado;
    }
    public int retonar_numero_procesos(){
        return this.ejecucion.size();
    }
    
    public boolean insertar_proceso(){
        try{
            Nodo proceso_nuevo;
            Random rand = new Random();
            if (retonar_numero_procesos()>0){
                tiempo_llegada = tiempo_llegada+rand.nextInt(5); //evita que el tiempo de llegada sea menor al de otro proceso
            }
            proceso_nuevo = new Nodo(ejecucion.size()+1,rand.nextInt(5)+1,tiempo_llegada);
            ejecucion.add(proceso_nuevo);
        }catch(Exception n){
            System.err.println(n);
            return false;
        }
        return true;
    }
    
    public void reordenarColaNodo(int n){
        try{
            for (int i = 0; i < ejecucion.size(); i++){
                if(ejecucion.get(i).tiempo_llegada<=n){
                    for (int j = i + 1; j < ejecucion.size(); j++){
                        if(ejecucion.get(j).tiempo_llegada<=n){
                            if(ejecucion.get(i).rafaga.get(ejecucion.get(i).rafaga.size()-1) > ejecucion.get(j).rafaga.get(ejecucion.get(j).rafaga.size()-1)){
                                Nodo aux = ejecucion.get(i);
                                ejecucion.set(i, ejecucion.get(j));
                                ejecucion.set(j, aux);
                            }
                        }
                    }
                }
            } 
        }catch (Exception e){
            System.err.println("Error en reordena"+e);
        }

    }
    
    public void añadir_finalizados(){
        int aux;
        try{
            for (int i = 0; i < ejecucion.size(); i++){
                if (ejecucion.get(i).rafaga.get(ejecucion.get(i).rafaga.size()-1)==0){
                    System.out.println(ejecucion.get(i).tiempo_final);
                    finalizado.add(ejecucion.get(i));
                    ejecucion.remove(i);
                }
            }
        }catch (Exception e){
            System.err.println("Error en finalizados"+e);
        }
    }
    public void imprimir_procesos_test(){
        for (int i = 0; i < ejecucion.size(); i++){
            System.out.println("proceso id: "+ejecucion.get(i).id+" Rafaga: "+ejecucion.get(i).rafaga.get(ejecucion.get(i).rafaga.size()-1)+" tiempo llegada: "+ejecucion.get(i).tiempo_llegada);
        }
    }
    
    public void imprimir_procesos_finalizados_test(){
        for (int i = 0; i < finalizado.size(); i++){
            System.out.println("-----------------------------------------------------------------------");
            System.out.println("proceso id: "+finalizado.get(i).id+" Rafaga: "+finalizado.get(i).rafaga.get(finalizado.get(i).rafaga.size()-1)+" tiempo llegada: "+finalizado.get(i).tiempo_llegada);
            System.out.println("rafagas: "+finalizado.get(i).rafaga.toString()+" comienzo: "+finalizado.get(i).tiempo_de_comienzo.toString()+" final"+finalizado.get(i).tiempo_final);
            System.out.println("Banderas ejecucion : "+finalizado.get(i).ejecución.toString());
        }
    }
    
    public void definir_variables_restantes(){
        for (int i = 0; i < finalizado.size(); i++){
            System.out.println(finalizado.get(i).id);
            finalizado.get(i).tiempo_de_retorno = finalizado.get(i).tiempo_final.get(finalizado.get(i).tiempo_final.size()-1)-finalizado.get(i).tiempo_llegada;
            finalizado.get(i).tiempo_de_espera = finalizado.get(i).tiempo_de_retorno - finalizado.get(i).rafaga.get(0);
        }
    }
    
    public void generar_banderas_ejecucion(){
        int tiempo_final = finalizado.get(finalizado.size()-1).tiempo_final.get(finalizado.get(finalizado.size()-1).tiempo_final.size()-1);
        for (int i = 0; i < finalizado.size(); i++){
            for (int n = 0; n < tiempo_final;n++){
                if(n >= finalizado.get(i).tiempo_llegada){
                    for (int j = 0; j<finalizado.get(i).tiempo_final.size();j++){
                        if( n > (finalizado.get(i).tiempo_de_comienzo.get(j)-1) && n < (finalizado.get(i).tiempo_final.get(j))){
                            finalizado.get(i).ejecución.add(true);
                            break;
                        }
                        if((j+1)>=finalizado.get(i).tiempo_final.size()&& n < finalizado.get(i).tiempo_final.get(finalizado.get(i).tiempo_final.size()-1)){
                            finalizado.get(i).ejecución.add(false);
                        }
                    }
                }
                if(finalizado.get(i).tiempo_llegada > n || n >= finalizado.get(i).tiempo_final.get(finalizado.get(i).tiempo_final.size()-1)){
                   finalizado.get(i).ejecución.add(null);
                }
            }
        }
    }
   
}

